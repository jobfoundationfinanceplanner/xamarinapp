﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancePlanner2.Model;
using FinancePlanner2.Repository.Interfaces;
using Newtonsoft.Json;
using FinancePlanner2.Services.Interfaces;

namespace FinancePlanner2.Services
{
    public class ConfigService : IConfigService
    {
        IConfigRepository _configRepository = null;

        public ConfigService(IConfigRepository configRespository)
        {
            _configRepository = configRespository;
        }

        SpendingPlanConfig IConfigService.SpendingPlanConfig
        {
            get
            {
                return GetConfig<SpendingPlanConfig>(ConfigName.SpendingPlan);
            }
            set
            {
                SetConfig(value, ConfigName.SpendingPlan);
            }
        }

        //retrieves the requested config from repository
        private T GetConfig<T>(ConfigName configName)
        {
            T config;
            string configJson;
            configJson = _configRepository.GetConfig(configName);
            config = JsonConvert.DeserializeObject<T>(configJson);
            return config;
        }

        //updates the specified config in repository
        private void SetConfig<T>(T value, ConfigName configName)
        {
            string dataStr = JsonConvert.SerializeObject(value, Formatting.Indented);
            _configRepository.UpdateConfigFile(configName, dataStr);
        }
    }
}
