﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancePlanner2.Model;

namespace FinancePlanner2.Services.Interfaces
{
    public interface IConfigService
    {
        SpendingPlanConfig SpendingPlanConfig { get; set; }
    }
}
