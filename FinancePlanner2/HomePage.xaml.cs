﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using FinancePlanner2.UserControls;
using FinancePlanner2.Services.Interfaces;

namespace FinancePlanner2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class HomePage : ContentPage
    {
        IServiceFactory _serviceFactory;

        public HomePage(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
            InitializeComponent();

            BindingContext = this;
            navbar.OnNavigate += Navbar_OnNavigate;

            if (Device.RuntimePlatform == Device.iOS)
            {
                Padding = new Thickness(0, 30, 0, 0); //prevents the banner and top of the app from overlapping in iOS apps
            }
            contentGrid.Children.Add(_serviceFactory.SpendingPlan);
        }

        //loads the selected page
        private void Navbar_OnNavigate(object sender, EventArgs e)
        {
            var navEventArgs = e as NavBarNavigateEventArgs;
            if (navEventArgs.NavButton == NavButton.SpendingPlan)
            {
                contentGrid.Children.RemoveAt(1);
                _serviceFactory.SpendingPlan.Parent = null;
                contentGrid.Children.Add(_serviceFactory.SpendingPlan);
            }
            else if (navEventArgs.NavButton == NavButton.Info)
            {
                contentGrid.Children.RemoveAt(1);
                _serviceFactory.Info.Parent = null;
                contentGrid.Children.Add(_serviceFactory.Info);
            }
        }
    }
}
