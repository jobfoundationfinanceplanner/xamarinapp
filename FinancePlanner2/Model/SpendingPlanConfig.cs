﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancePlanner2.UserControls;
using Newtonsoft.Json;

namespace FinancePlanner2.Model
{
    //Represents user data
    public class SpendingPlanConfig
    {
        [JsonProperty("income")]
        public Income Income { get; set; }

        [JsonProperty("categories")]
        public List<Category> Categories { get; set; }
    }

    //Represents the user's income and how often they get paid
    public class Income
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("timeFrame")]
        public string TimeFrame { get; set; }
    }

    //Represents a high level category (e.g. savings, checking, cash)
    public class Category
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("subcategories")]
        public List<Subcategory> Subcategories { get; set; }
    }

    //Represents a category within the high level category (e.g. groceries, investment savings)
    public class Subcategory
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("where")]
        public string Where { get; set; }

        [JsonProperty("amount")]
        public JsonAmount JsonAmount { get; set; }

        //Represents the designated amount of money in dollars or percent
        public Amount Amount { get; set; }

        [JsonProperty("transactions")]
        public List<Transaction> Transactions { get; set; }
    }

    //Represents a deposit or withdrawal within a subcategory
    public class Transaction
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("date")]
        public string DateString { get; set; }

        public DateTime Date { get; set; }

        [JsonProperty("reconciled")]
        public bool Reconciled { get; set; }

        //to be converted to enum Type
        [JsonProperty("type")]
        public string TypeString { get; set; }

        //deposit or withdrawal
        public TransactionType Type { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("medium")]
        public string Medium { get; set; }

        //for withdrawals only
        [JsonProperty("notFromIncome")]
        public bool NotFromIncome { get; set; }
    }

    //Represents an Amount without enumerations, to be converted to an Amount
    public class JsonAmount
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("number")]
        public decimal Number { get; set; }
    }
}
