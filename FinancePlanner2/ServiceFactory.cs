﻿using System;
using Autofac;
using FinancePlanner2.UserControls;
using FinancePlanner2.Repository;
using FinancePlanner2.Repository.Interfaces;
using FinancePlanner2.Services;
using FinancePlanner2.Services.Interfaces;

namespace FinancePlanner2
{
    public interface IServiceFactory
    {
        HomePage HomePage { get; }
        SpendingPlan SpendingPlan { get; }
        Info Info { get; }
        SpendingPlanCategory CreateSpendingPlanCategory();
        SpendingPlanPopup CreateSpendingPlanPopup();
        TransactionsPopup CreateTransactionsPopup();
        SpendingPlanEdit CreateSpendingPlanEdit();
        TransactionEdit CreateTransactionEdit();
        SpendingPlanIncome CreateSpendingPlanIncome();
    }
    public class ServiceFactory : IServiceFactory
    {
        private IContainer _container;
        public ServiceFactory()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterInstance<IServiceFactory>(this).SingleInstance();
            containerBuilder.RegisterType<ConfigRepository>().As<IConfigRepository>().SingleInstance();
            containerBuilder.RegisterType<ConfigService>().As<IConfigService>().SingleInstance();
            containerBuilder.RegisterType<HomePage>().SingleInstance();
            containerBuilder.RegisterType<SpendingPlan>().SingleInstance();
            containerBuilder.RegisterType<Info>().SingleInstance();
            containerBuilder.RegisterType<SpendingPlanCategory>();
            containerBuilder.RegisterType<SpendingPlanPopup>();
            containerBuilder.RegisterType<TransactionsPopup>();
            containerBuilder.RegisterType<SpendingPlanEdit>();
            containerBuilder.RegisterType<TransactionEdit>();
            containerBuilder.RegisterType<SpendingPlanIncome>();

            _container = containerBuilder.Build();
        }

        public SpendingPlanCategory CreateSpendingPlanCategory()
        {
            return _container.Resolve<SpendingPlanCategory>();
        }

        public SpendingPlanPopup CreateSpendingPlanPopup()
        {
            return _container.Resolve<SpendingPlanPopup>();
        }

        public TransactionsPopup CreateTransactionsPopup()
        {
            return _container.Resolve<TransactionsPopup>();
        }

        public SpendingPlanEdit CreateSpendingPlanEdit()
        {
            return _container.Resolve<SpendingPlanEdit>();
        }

        public TransactionEdit CreateTransactionEdit()
        {
            return _container.Resolve<TransactionEdit>();
        }

        public SpendingPlanIncome CreateSpendingPlanIncome()
        {
            return _container.Resolve<SpendingPlanIncome>();
        }

        public HomePage HomePage
        {
            get
            {
                return _container.Resolve<HomePage>();
            }
        }

        public SpendingPlan SpendingPlan
        {
            get
            {
                return _container.Resolve<SpendingPlan>();
            }
        }

        public Info Info
        {
            get
            {
                return _container.Resolve<Info>();
            }
        }
    }
}
