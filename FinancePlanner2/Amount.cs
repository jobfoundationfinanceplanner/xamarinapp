﻿using System;
namespace FinancePlanner2
{
    public enum TransactionType { withdrawal, deposit };
    public enum AmountType { dollars, percent };

    //represents an amount of money in dollars or percent to be designated to a subcategory
    public class Amount
    {
        public decimal Number { get; set; }
        public AmountType Type { get; set; }
        public Amount(decimal number, AmountType type)
        {
            Number = number;
            Type = type;
        }
    }
}
