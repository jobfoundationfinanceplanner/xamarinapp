﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancePlanner2.Model;
using FinancePlanner2.Services.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpendingPlanEdit : ContentView
    {
        public event EventHandler SaveConfig;

        //IServiceFactory _serviceFactory;
        public Subcategory Subcategory { get; set; }
        public int ParentID { get; set; }
        public SpendingPlanConfig Config;

        public SpendingPlanEdit()
        {
            InitializeComponent();
        }

        //populates the user input fields with the subcategory info
        public void PopulateFields()
        {
            if(Subcategory.Name != "New category") //user shouldn't have to manually delete the default value before entering a new one
            {
                nameEntry.Text = Subcategory.Name;
            }
            if (Subcategory.Where != "")
            {
                whereEntry.Text = Subcategory.Where;
            }
            if(Subcategory.Amount.Number != 0)
            {
                amountEntry.Text = Subcategory.Amount.Number.ToString("0.00");
            }
            amountTypeEntry.ItemsSource = new List<string> { "Dollars ($)", "Percent (%)" };
            amountTypeEntry.SelectedIndex = (Subcategory.Amount.Type == AmountType.dollars) ? 0 : 1;
            //amountType.Text = (Subcategory.Amount.Type == AmountType.dollars) ? "Dollars ($)" : "Percent (%)";
        }

        //closes window
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            ClosePopup();
        }

        //makes a new Subcategory object from user input, saves, and closes window
        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            string name = nameEntry.Text;
            string where = whereEntry.Text;
            string amountString = amountEntry.Text;
            int amountTypeIndex = amountTypeEntry.SelectedIndex;

            if (name == "" || name == null) //if name is empty, reverts to whatever it was before
            {
                name = Subcategory.Name;
            }

            decimal amountNum;
            try
            {
                amountNum = Convert.ToDecimal(amountString);
                amountNum = Math.Floor(amountNum * 100)/100;
            }
            catch (FormatException) //if user enters something funky, revert to whatever it was before
            {
                amountNum = Subcategory.Amount.Number;
            }

            AmountType amountType;
            if(amountTypeIndex == 0)
            {
                amountType = AmountType.dollars;
            }
            else
            {
                amountType = AmountType.percent;
            }

            Amount amount = new Amount(amountNum, amountType);

            JsonAmount jsonAmount = new JsonAmount();
            jsonAmount.Number = amountNum;
            jsonAmount.Type = amountType.ToString();

            //creates the new Subcategory object
            Subcategory newSc = new Subcategory();
            newSc.Id = Subcategory.Id;
            newSc.Name = name;
            newSc.Where = where;
            newSc.JsonAmount = jsonAmount;
            newSc.Amount = amount;

            if(Subcategory.Transactions != null)
            {
                newSc.Transactions = Subcategory.Transactions;
            }
            else
            {
                newSc.Transactions = new List<Transaction>();
            }

            //puts the new Subcategory object into the config
            if(newSc.Id < Config.Categories[ParentID].Subcategories.Count() && newSc.Id >= 0)
            {
                Config.Categories[ParentID].Subcategories[Subcategory.Id] = newSc;
            }
            else
            {
                Config.Categories[ParentID].Subcategories.Add(newSc);
            }

            //save
            SaveConfig?.Invoke(this, null);

            //close window
            ClosePopup();

        }

        //closes window
        private void ClosePopup()
        {
            popupLoadingView.IsVisible = false;
            popupLoadingView.InputTransparent = true;
            content.Children.Clear();
        }
    }
}