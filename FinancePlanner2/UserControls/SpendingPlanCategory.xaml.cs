﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancePlanner2.Model;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2.UserControls
{
    public enum CategoryType {income, primary, secondary, transaction};
    

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpendingPlanCategory : Grid
    {
        public event EventHandler SpendingPlanPopup;
        public event EventHandler DeleteCategory;

        public SpendingPlanConfig Config { get; set; }
        public CategoryType CategoryType { get; set; }
        public Income Income { get; set; }
        public Category Category { get; set; }
        public Subcategory Subcategory { get; set; }
        public Transaction Transaction { get; set; }
        public int ParentID;
        private bool editMode;

        public SpendingPlanCategory()
        {
            InitializeComponent();
        }

        //Loads the category information into the appropriate template (income, main category, subcategory, or transaction)
        public void Update()
        {
            //income - used in SpendingPlan
            if(CategoryType == CategoryType.income)
            {
                categoryName.Text = "Income";
                categoryDesc.Text = $"${Income.Amount:0.00} per {Income.TimeFrame}";
                categoryGrid.BackgroundColor = Color.FromHex("#ccffcc");
            }

            //high-level category - used in SpendingPlan
            else if (CategoryType == CategoryType.primary)
            {
                categoryName.Text = Category.Name;

                int numItems = Category.Subcategories.Count;
                string itemOrItems = numItems == 1 ? "item" : "item";
                decimal totalAmount = 0;
                decimal totalPercent = 0;

                //iterates through all subcategories to find the total amount of money assigned
                foreach (Subcategory subcategory in Category.Subcategories)
                {
                    if(subcategory.Amount.Type == AmountType.dollars)
                    {
                        totalAmount += subcategory.Amount.Number;
                        totalPercent += (subcategory.Amount.Number / Income.Amount) * 100; //Calculate percentage from dollars
                    }
                    else
                    {
                        totalAmount += (subcategory.Amount.Number / 100) * Income.Amount; //Calculate dollars from percentage
                        totalPercent += subcategory.Amount.Number;
                    }
                }

                categoryDesc.Text = $"{numItems} {itemOrItems}\t ${totalAmount:0.00}\t {totalPercent:0.00}%";
            }

            //subcategory - used in SpendingPlanPopup
            else if (CategoryType == CategoryType.secondary)
            {
                categoryName.Text = Subcategory.Name;
                categoryName.FontSize = 18;

                string amountString;
                if (Subcategory.Amount.Type == AmountType.dollars)
                {
                    amountString = $"${Subcategory.Amount.Number:0.00}";
                }
                else
                {
                    amountString = $"{Subcategory.Amount.Number:0.00}%";
                }

                categoryDesc.Text = $"{amountString}\t{Subcategory.Where}";
            }

            //transaction - used in TransactionsPopup
            else if (CategoryType == CategoryType.transaction)
            {
                categoryName.Text = Transaction.Description;
                categoryName.FontSize = 18;

                string plusOrMinus = "-";
                if (Transaction.Type == TransactionType.deposit)
                {
                    plusOrMinus = "+";
                    categoryGrid.BackgroundColor = Color.FromHex("#ccffcc");
                }

                string dateString = Transaction.Date.ToShortDateString();
                string reconciledString = Transaction.Reconciled ? "Reconciled" : "Not reconciled";
                categoryDesc.Text = $"{plusOrMinus}${Transaction.Amount:0.00}\t {dateString}\t {reconciledString}";
            }
        }

        //Enables or disables the ability to delete the category
        public void ToggleEditMode(bool _editMode)
        {
            if(CategoryType == CategoryType.income) //income can't be deleted
            {
                return;
            }
            editMode = _editMode;
            button.InputTransparent = _editMode;
            arrow.IsVisible = !_editMode;
            minusButton.InputTransparent = !_editMode;
            minusButton.IsVisible = _editMode;
            deleteButton.InputTransparent = true;
            deleteButton.IsVisible = false;
        }

        //If edit mode is disable, creates a popup
        private void Button_Clicked(object sender, EventArgs e)
        {
            if(!editMode)
            {
                SpendingPlanPopup?.Invoke(this, null);
            }
            else //failsafe: shouldn't be clickable with edit mode on
            {
                ToggleEditMode(true);
            }
        }

        //enables the delete button
        private void MinusButton_Clicked(object sender, EventArgs e)
        {
            minusButton.IsVisible = false;
            minusButton.InputTransparent = true;
            deleteButton.IsVisible = true;
            deleteButton.InputTransparent = false;
            button.InputTransparent = false;
        }

        //deletes the category
        private void DeleteButton_Clicked(object sender, EventArgs e)
        {
            DeleteCategory?.Invoke(this, null);
        }
    }
}