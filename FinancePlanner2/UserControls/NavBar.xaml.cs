﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancePlanner2;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2.UserControls
{
    public enum NavButton
    {
        SpendingPlan, Info
    }

    public class NavBarNavigateEventArgs : EventArgs
    {
        public NavButton NavButton { get; set; }

        public NavBarNavigateEventArgs(NavButton navButton)
        {
            NavButton = navButton;
        }

    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavBar : Grid
    {
        IServiceFactory _serviceFactory = null;

        public event EventHandler OnNavigate;
        public NavBar(IServiceFactory serviceFactory = null)
        {
            _serviceFactory = serviceFactory;
            InitializeComponent();
        }

        public NavBar()
        {
            _serviceFactory = App.ServiceFactory;
            InitializeComponent();
            UpdateButtons(spendingPlanButton);
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            navLayout.WidthRequest = this.Width;
            navGrid.WidthRequest = this.Width;
            base.OnSizeAllocated(width, height);
        }

        //navigates to SpendingPlan.xaml
        private void spendingPlanButton_Clicked(object sender, System.EventArgs e)
        {
            OnNavigate?.Invoke(this, new NavBarNavigateEventArgs(NavButton.SpendingPlan));
            UpdateButtons((Button)sender);
        }

        //navigates to Info.xaml
        private void infoButton_Clicked(object sender, System.EventArgs e)
        {
            OnNavigate?.Invoke(this, new NavBarNavigateEventArgs(NavButton.Info));
            UpdateButtons((Button)sender);

        }

        //Highlights the button for the selected tab
        //use the commented code when icons are added
        private void UpdateButtons(Button btn)
        {
            var selectedThickness = new Thickness(0, -10, 0, 0);
            var thickness = new Thickness(0, 0, 0, 0);
            spendingPlanButton.Margin = thickness; //spendingPlanButton.ImageSource = "spendingPlan";
            infoButton.Margin = thickness; //infoButton.ImageSource = "info";

            btn.Margin = selectedThickness;
            /*if (btn == spendingPlanButton) { spendingPlanButton.ImageSource = "spendingPlanS"; }
            else if (btn == transactionsButton) { transactionsButton.ImageSource = "transactionsS"; }
            else if (btn == historyButton) { historyButton.ImageSource = "historyS"; }
            else if (btn == infoButton) { infoButton.ImageSource = "infoS"; }*/
        }
    }
}