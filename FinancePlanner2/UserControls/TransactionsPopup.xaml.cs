﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinancePlanner2.Model;
using Xamarin.Forms;

namespace FinancePlanner2.UserControls
{
    public partial class TransactionsPopup : ContentView
    {
        public event EventHandler SaveConfig;
        public event EventHandler PopupUpdate;
        public event EventHandler PopupClosed;

        IServiceFactory _serviceFactory;
        public Subcategory Subcategory { get; set; }
        public int ParentID { get; set; }
        public SpendingPlanConfig Config;
        private bool editMode = false;
        private ImageButton plusSign;

        public TransactionsPopup(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
            CreatePlusSign();
            InitializeComponent();
        }

        //creates a plus sign to be placed later
        private void CreatePlusSign()
        {
            //plusSign = _serviceFactory.CreatePlusSign();
            plusSign = new ImageButton();
            plusSign.Source = "plussign";
            plusSign.BackgroundColor = Color.Transparent;
            plusSign.HeightRequest = 35;
            plusSign.WidthRequest = 35;
            plusSign.Margin = 5;
            plusSign.Clicked += PlusSign_Clicked;
            plusSign.IsVisible = false;
            plusSign.InputTransparent = true;
        }

        //creates categories representing transactions
        public void CreateTransactions()
        {
            title.Text = Subcategory.Name;
            foreach (Transaction transaction in Subcategory.Transactions)
            {
                CreateTransaction(transaction);
            }
            //append plus sign at the end
            elements.Children.Add(plusSign);

            //Show the plus sign if there are no transactions to show, even if edit mode isn't enabled
            if (Subcategory.Transactions.Count() == 0)
            {
                plusSign.IsVisible = true;
                plusSign.InputTransparent = false;
            }
        }

        //creates a category representing a transaction
        private void CreateTransaction(Transaction transaction)
        {
            SpendingPlanCategory category = _serviceFactory.CreateSpendingPlanCategory();
            category.CategoryType = CategoryType.transaction;
            category.Transaction = transaction;
            category.Update();
            category.ToggleEditMode(editMode);
            category.SpendingPlanPopup += Category_Clicked;
            category.DeleteCategory += DeleteTransaction;
            elements.Children.Add(category);
        }

        //lets the user edit the Subcategory
        private void Title_Clicked(object sender, EventArgs e)
        {
            MakeSpendingPlanEdit();
        }

        bool MakeCalled = false;
        //makes a window for the user to edit the Subcategory
        private void MakeSpendingPlanEdit()
        {
            SpendingPlanEdit popup = _serviceFactory.CreateSpendingPlanEdit();
            popup.Subcategory = Subcategory;
            popup.ParentID = ParentID;
            popup.Config = Config;
            popup.PopulateFields();
            popup.SaveConfig += _SaveConfig;
            Grid container = content;

            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;
            container.Children.Add(popup);
        }

        //makes a window for the user to edit the selected transaction
        private void MakeTransactionEdit(Transaction transaction)
        {
            TransactionEdit popup = _serviceFactory.CreateTransactionEdit();
            popup.Transaction = transaction;
            popup.CategoryID = ParentID;
            popup.SubcategoryID = Subcategory.Id;
            popup.Config = Config;
            popup.PopulateFields();
            popup.SaveConfig += _SaveConfig;
            Grid container = content;

            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;
            container.Children.Add(popup);
        }

        //closes window
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = false;
            popupLoadingView.InputTransparent = true;
            PopupClosed?.Invoke(this, null);
        }

        //enables edit mode
        private void EditButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(true);
        }

        //disables edit mode and saves changes
        private void DoneButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(false);
            _SaveConfig(null, null);
        }

        //creates a new transaction and makes a window for editing it
        private void PlusSign_Clicked(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction();
            transaction.Id = Subcategory.Transactions.Count();
            transaction.Description = "New transaction";
            transaction.Amount = 0;
            transaction.Date = DateTime.Today;
            transaction.Medium = "";
            transaction.NotFromIncome = false;
            transaction.Reconciled = false;
            transaction.Type = TransactionType.withdrawal;
            MakeTransactionEdit(transaction);
        }

        //enables or disables edit mode for adding or removing transactions
        private void ToggleEditMode(bool _editMode)
        {
            editMode = _editMode;

            backButton.IsVisible = !_editMode;
            backButton.InputTransparent = _editMode;
            editButton.IsVisible = !_editMode;
            editButton.InputTransparent = _editMode;
            doneButton.IsVisible = _editMode;
            doneButton.InputTransparent = !_editMode;
            plusSign.IsVisible = _editMode;
            plusSign.InputTransparent = !_editMode;
            title.IsVisible = !_editMode;

            //toggle edit mode for each category on screen - allows the user to delete them
            foreach (View category in elements.Children)
            {
                if (category is SpendingPlanCategory)
                {
                    ((SpendingPlanCategory)category).ToggleEditMode(_editMode);
                }
            }
        }

        //lets the user edit the selected transaction
        private void Category_Clicked(object sender, EventArgs e)
        {
            SpendingPlanCategory element = sender as SpendingPlanCategory;
            Transaction transaction = element.Transaction;
            MakeTransactionEdit(transaction);
        }

        //deletes the transaction from the config and the screen
        private void DeleteTransaction(object sender, EventArgs e)
        {
            SpendingPlanCategory category = sender as SpendingPlanCategory;
            Subcategory.Transactions.RemoveAt(category.Transaction.Id);

            //change the IDs of subcategories to match their new indices
            for (int i = category.Transaction.Id; i < Subcategory.Transactions.Count(); i++)
            {
               Subcategory.Transactions[i].Id--;
            }

            _SaveConfig(null, null);
        }

        //saves changes and updates the screen
        private void _SaveConfig(object sender, EventArgs e)
        {
            Subcategory = Config.Categories[ParentID].Subcategories[Subcategory.Id];
            elements.Children.Clear();
            CreateTransactions();
            SaveConfig?.Invoke(this, null);
            PopupUpdate?.Invoke(this, null);
        }
    }
}
