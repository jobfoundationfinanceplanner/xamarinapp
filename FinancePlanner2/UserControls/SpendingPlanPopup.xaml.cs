﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FinancePlanner2.Model;
using FinancePlanner2.Services.Interfaces;

namespace FinancePlanner2.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpendingPlanPopup : ContentView
    {
        public event EventHandler SaveConfig;
        public event EventHandler UpdateBalanceBanner;

        IServiceFactory _serviceFactory;
        public SpendingPlanConfig Config { get; set; }
        public int ID { get; set; }
        public Subcategory SelectedSc { get; set; }
        private bool editMode = false;
        private ImageButton plusSign;

        public SpendingPlanPopup(IServiceFactory serviceFactory)
        {
            _serviceFactory = serviceFactory;
            CreatePlusSign();
            InitializeComponent();
        }

        //creates a plus sign to be placed later
        private void CreatePlusSign()
        {
            //plusSign = _serviceFactory.CreatePlusSign();
            plusSign = new ImageButton();
            plusSign.Source = "plussign";
            plusSign.BackgroundColor = Color.Transparent;
            plusSign.HeightRequest = 35;
            plusSign.WidthRequest = 35;
            plusSign.Margin = 5;
            plusSign.Clicked += PlusSign_Clicked;
            plusSign.IsVisible = false;
            plusSign.InputTransparent = true;
        }

        //make categories representing Subcategory objects
        public void CreateCategories()
        {
            title.Text = titleEntry.Text = Config.Categories[ID].Name;

            //if title is default value, makes title editable, even if edit mode isn't enabled
            if (title.Text.Contains("New category")) 
            {
                titleEntry.Text = string.Empty;
                titleEntry.Placeholder = title.Text;
                title.IsVisible = false;
                titleEntry.IsVisible = true;
                titleEntry.InputTransparent = false;
            }

            foreach (Subcategory category in Config.Categories[ID].Subcategories)
            {
                CreateCategory(category);
            }
            //append plus sign at the end
            elements.Children.Add(plusSign);

            //Show the plus sign if there are no categories to show, even if edit mode isn't enabled
            if (Config.Categories[ID].Subcategories.Count() == 0)
            {
                plusSign.IsVisible = true;
                plusSign.InputTransparent = false;
            }
        }

        //make a category representing a Subcategory object
        private void CreateCategory(Subcategory subcategoryData)
        {
            SpendingPlanCategory category = _serviceFactory.CreateSpendingPlanCategory();
            category.CategoryType = CategoryType.secondary;
            category.Income = Config.Income;
            category.Subcategory = subcategoryData;
            category.Update();
            category.ToggleEditMode(editMode);
            category.SpendingPlanPopup += Category_Clicked;
            category.DeleteCategory += DeleteCategory;
            elements.Children.Add(category);
        }

        //close window
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            popupLoadingView.IsVisible = false;
            popupLoadingView.InputTransparent = true;
        }

        //enable edit mode
        private void EditButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(true);
        }

        //save changes to title and disable edit mode
        private void DoneButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(false);
            SaveTitle();
        }

        //create a new Subcategory and open a page to edit it
        private void PlusSign_Clicked(object sender, EventArgs e)
        {
            Subcategory subcategory = new Subcategory();
            subcategory.Amount = new Amount(0, AmountType.dollars);
            subcategory.Id = Config.Categories[ID].Subcategories.Count();
            subcategory.Name = "New category";
            subcategory.Where = "";
            MakeSpendingPlanEdit(subcategory);
        }

        //enables or disables edit mode for adding or deleting Subcategories
        private void ToggleEditMode(bool _editMode)
        {
            editMode = _editMode;

            backButton.IsVisible = !_editMode;
            backButton.InputTransparent = _editMode;
            editButton.IsVisible = !_editMode;
            editButton.InputTransparent = _editMode;
            doneButton.IsVisible = _editMode;
            doneButton.InputTransparent = !_editMode;
            plusSign.IsVisible = _editMode;
            plusSign.InputTransparent = !_editMode;
            title.IsVisible = !_editMode;
            titleEntry.IsVisible = _editMode;
            titleEntry.InputTransparent = !_editMode;

            //if title is default value, makes title editable, even if edit mode isn't enabled
            if (title.Text.Contains("New category"))
            {
                title.IsVisible = false;
                titleEntry.IsVisible = true;
                titleEntry.InputTransparent = false;
            }

            //toggle edit mode for each category on screen - allows the user to delete them
            foreach (View category in elements.Children)
            {
                if(category is SpendingPlanCategory)
                {
                    ((SpendingPlanCategory)category).ToggleEditMode(_editMode);
                }
            }
        }

        //opens a window with the transactions within the selected Subcategory
        private void Category_Clicked(object sender, EventArgs e)
        {
            SpendingPlanCategory element = sender as SpendingPlanCategory;
            Subcategory subcategory = element.Subcategory;
            MakeTransactionsPopup(subcategory);
            SelectedSc = subcategory;
            UpdateBalanceBanner?.Invoke(this, null);
        }

        bool MakeCalled = false;

        //makes a new window with the transactions within subcategory
        private void MakeTransactionsPopup(Subcategory subcategory)
        {

            TransactionsPopup popup = _serviceFactory.CreateTransactionsPopup();
            popup.Subcategory = subcategory;
            popup.ParentID = ID;
            popup.Config = Config;
            popup.CreateTransactions();
            popup.SaveConfig += _SaveConfig;
            popup.PopupUpdate += PopupUpdate;
            popup.PopupClosed += PopupClosed;
            Grid container = content;

            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;
            container.Children.Add(popup);
        }

        //makes a new window for editing subcategory
        private void MakeSpendingPlanEdit(Subcategory subcategory)
        {
            SpendingPlanEdit popup = _serviceFactory.CreateSpendingPlanEdit();
            popup.Subcategory = subcategory;
            popup.ParentID = ID;
            popup.Config = Config;
            popup.PopulateFields();
            popup.SaveConfig += _SaveConfig;
            Grid container = content;

            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;
            container.Children.Add(popup);
        }

        //deletes the selected Subcategory from the config and screen
        private void DeleteCategory(object sender, EventArgs e)
        {
            SpendingPlanCategory category = sender as SpendingPlanCategory;
            Config.Categories[ID].Subcategories.RemoveAt(category.Subcategory.Id);

            //change the IDs of subcategories to match their new indices
            for(int i = category.Subcategory.Id; i < Config.Categories[ID].Subcategories.Count(); i++)
            {
                Config.Categories[ID].Subcategories[i].Id--;
            }

            _SaveConfig(null, null);
        }

        //saves the new title to the config and screen
        private void SaveTitle()
        {
            if(title.Text != titleEntry.Text && titleEntry.Text.Trim() != string.Empty)
            {
                title.Text = titleEntry.Text;
                Config.Categories[ID].Name = titleEntry.Text;
                SaveConfig?.Invoke(this, null);
            }
            else
            {
                titleEntry.Text = title.Text;
            }
        }

        //saves the updated config to repository and refreshes screen
        private void _SaveConfig(object sender, EventArgs e)
        {
            elements.Children.Clear();
            CreateCategories();
            SaveConfig?.Invoke(this, null);
        }

        //updates the subcategory balance banner
        private void PopupUpdate(object sender, EventArgs e)
        {
            UpdateBalanceBanner?.Invoke(this, null);
        }

        //clears the subcategory balance banner
        private void PopupClosed(object sender, EventArgs e)
        {
            UpdateBalanceBanner?.Invoke(null, null);
        }
    }
}