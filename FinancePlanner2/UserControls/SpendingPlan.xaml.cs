﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FinancePlanner2.Services.Interfaces;
using FinancePlanner2.Model;

namespace FinancePlanner2.UserControls
{
    //replaced with user input
    //public enum TimeFrame { month, week, twoWeeks, thisTime };

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpendingPlan : Grid
    {
        IConfigService _configService;
        IServiceFactory _serviceFactory;
        private SpendingPlanConfig config;
        private bool editMode = false;
        private ImageButton plusSign;

        public SpendingPlan(IConfigService configService, IServiceFactory serviceFactory)
        {
            _configService = configService;
            _serviceFactory = serviceFactory;
            CreatePlusSign();
            InitializeComponent();
            config = SetUpConfig(_configService.SpendingPlanConfig);
            CreateCategories();
            UpdateAmountsRemaining();
        }

        //creates a plus sign to be placed later
        private void CreatePlusSign()
        {
            //plusSign = _serviceFactory.CreatePlusSign();
            plusSign = new ImageButton();
            plusSign.Source = "plussign";
            plusSign.BackgroundColor = Color.Transparent;
            plusSign.HeightRequest = 35;
            plusSign.WidthRequest = 35;
            plusSign.Margin = 5;
            plusSign.Clicked += PlusSign_Clicked;
            plusSign.IsVisible = false;
            plusSign.InputTransparent = true;
        }

        //Creates clickable categories
        private void CreateCategories()
        {
            CreateIncomeCategory();
            foreach (Category category in config.Categories)
            {
                CreateCategory(category);
            }
            //Append the plus sign at the end
            categories.Children.Add(plusSign);

            //Show the plus sign if there are no categories to show, even if edit mode isn't enabled
            if (config.Categories.Count() == 0)
            {
                plusSign.IsVisible = true;
                plusSign.InputTransparent = false;
            }
        }

        //Creates a clickable income category
        private void CreateIncomeCategory()
        {
            SpendingPlanCategory category = _serviceFactory.CreateSpendingPlanCategory();
            category.CategoryType = CategoryType.income;
            category.Income = config.Income;
            category.Update();
            category.SpendingPlanPopup += Category_Clicked;
            categories.Children.Add(category);
        }

        //Creates a clickable category
        private void CreateCategory(Category categoryData)
        {
            SpendingPlanCategory category = _serviceFactory.CreateSpendingPlanCategory();
            category.CategoryType = CategoryType.primary;
            category.Income = config.Income;
            category.Category = categoryData;
            category.Update();
            category.ToggleEditMode(editMode);
            category.SpendingPlanPopup += Category_Clicked;
            category.DeleteCategory += DeleteCategory;
            categories.Children.Add(category);
        }

        //Update the banners at the bottom of the page
        private void UpdateAmountsRemaining()
        {
            UpdateBalance();
            UpdateIncomeRemaining();
            UpdateAmountUnassigned();
        }

        //Updates the banner with the total balance
        private void UpdateBalance()
        {
            decimal balance = 0;
            //iterates through all transactions to calculate the total balance
            foreach (Category category in config.Categories)
            {
                foreach (Subcategory subcategory in category.Subcategories)
                {
                    foreach (Transaction transaction in subcategory.Transactions)
                    {
                        if(transaction.Type == TransactionType.deposit)
                        {
                            balance += transaction.Amount;
                        }
                        else
                        {
                            balance -= transaction.Amount;
                        }
                    }
                }
            }
            string plusOrMinus = balance < 0 ? "-" : "+";
            totalBalanceLabel.Text = $"Total balance: {plusOrMinus}${Math.Abs(balance):0.00}";

            //if the balance is below 0 the banner appears red
            if (balance < 0)
            {
                totalBalance.BackgroundColor = Color.FromHex("#ff8080");
            }
            else
            {
                totalBalance.BackgroundColor = Color.Transparent;
            }
        }

        //Updates the banner with how much of the income hasn't been used
        private void UpdateIncomeRemaining()
        {
            decimal income = config.Income.Amount;
            decimal amountRemaining = income;
            //iterates through all withdrawals and subtracts them from the total income
            foreach (Category category in config.Categories)
            {
                foreach (Subcategory subcategory in category.Subcategories)
                {
                    foreach (Transaction transaction in subcategory.Transactions)
                    {
                        if (transaction.Type == TransactionType.withdrawal && !transaction.NotFromIncome)
                        {
                            amountRemaining -= transaction.Amount;
                        }
                    }
                }
            }
            incomeRemainingLabel.Text = $"${amountRemaining:0.00} remaining (based on income)";

            //if the income remaining is below 0 the banner appears red
            if (amountRemaining < 0)
            {
                incomeRemaining.BackgroundColor = Color.FromHex("#ff8080");
            }
            else
            {
                incomeRemaining.BackgroundColor = Color.Transparent;
            }
        }

        //Updates the banner with how much income hasn't been assigned to a subcategory
        private void UpdateAmountUnassigned()
        {
            decimal income = config.Income.Amount;
            decimal amountRemaining = income;
            //iterates through all subcategories to calculate how much income hasn't been assigned
            foreach (Category category in config.Categories)
            {
                foreach (Subcategory subcategory in category.Subcategories)
                {
                    if (subcategory.Amount.Type == AmountType.percent)
                    {
                        amountRemaining -= income * (subcategory.Amount.Number / 100);
                    }
                    else
                    {
                        amountRemaining -= subcategory.Amount.Number;
                    }
                }
            }
            decimal percentRemaining = (amountRemaining / income) * 100;
            dollarsUnassignedLabel.Text = $"${amountRemaining:0.00} unassigned";
            percentUnassignedLabel.Text = $"{percentRemaining:0.00}% unassigned";

            //if the amount unassigned is below 0 the banner appears red
            if (amountRemaining < 0)
            {
                amountsUnassigned.BackgroundColor = Color.FromHex("#ff8080");
            }
            else
            {
                amountsUnassigned.BackgroundColor = Color.Transparent;
            }
        }

        //Updates the topmost banner, representing the balance of the selected subcategory
        private void UpdateBalanceBanner(object sender, EventArgs e)
        {
            //if no subcategory is selected, banner should be blank
            if(sender == null)
            {
                categoryBalanceLabel.Text = "";
                categoryBalance.BackgroundColor = Color.Transparent;
                return;
            }

            SpendingPlanPopup spendingPlanPopup = sender as SpendingPlanPopup;
            string name = spendingPlanPopup.SelectedSc.Name;
            decimal balance = 0;
            //iterates through all transactions in the selected subcategory to calculate the balance
            foreach (Transaction transaction in spendingPlanPopup.SelectedSc.Transactions)
            {
                if (transaction.Type == TransactionType.deposit)
                {
                    balance += transaction.Amount;
                }
                else
                {
                    balance -= transaction.Amount;
                }
            }

            string plusOrMinus = balance < 0 ? "-" : "+";
            categoryBalanceLabel.Text = $"{name} balance: {plusOrMinus}${Math.Abs(balance):0.00}";

            //if the balance is below 0 the banner appears red
            if (balance < 0)
            {
                categoryBalance.BackgroundColor = Color.FromHex("#ff8080");
            }
            else
            {
                categoryBalance.BackgroundColor = Color.Transparent;
            }
        }

        //turns on edit mode
        private void EditButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(true);
        }

        //turns off edit mode
        private void DoneButton_Clicked(object sender, EventArgs e)
        {
            ToggleEditMode(false);
        }

        //Creates an empty category and opens it
        private void PlusSign_Clicked(object sender, EventArgs e)
        {
            Category category = new Category();
            category.Name = "New category";
            category.Id = config.Categories.Count();
            category.Subcategories = new List<Subcategory>();
            config.Categories.Add(category);
            SaveConfig(null, null);
            MakeCategoryPopup(category.Id);
        }

        //Opens the selected category
        private void Category_Clicked(object sender, EventArgs e)
        {
            SpendingPlanCategory element = sender as SpendingPlanCategory;
            if (element.CategoryType == CategoryType.income)
            {
                MakeIncomePopup();
            }
            else
            {
                MakeCategoryPopup(element.Category.Id);
            }
        }

        //Enables or disables edit mode, for deleting or adding categories
        private void ToggleEditMode(bool _editMode)
        {
            editMode = _editMode;

            editButton.IsVisible = !_editMode;
            editButton.InputTransparent = _editMode;
            doneButton.IsVisible = _editMode;
            doneButton.InputTransparent = !_editMode;
            plusSign.IsVisible = _editMode;
            plusSign.InputTransparent = !_editMode;

            //toggle edit mode for each category on screen - allows the user to delete them
            foreach (View category in categories.Children)
            {
                if (category is SpendingPlanCategory)
                {
                    ((SpendingPlanCategory)category).ToggleEditMode(_editMode);
                }
            }
        }

        bool MakeCalled = false;
        //Loads a screen to edit income
        private void MakeIncomePopup()
        {
            Grid container = content;
            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;

            SpendingPlanIncome popup = _serviceFactory.CreateSpendingPlanIncome();
            popup.Income = config.Income;
            popup.Config = config;
            popup.PopulateFields();
            popup.SaveConfig += SaveConfig;
            container.Children.Add(popup);
        }

        //Loads a screen to view/edit the category
        private void MakeCategoryPopup(int categoryId)
        {
            Grid container = content;
            if (MakeCalled == true) { container.Children.RemoveAt(1); }
            MakeCalled = true;

            SpendingPlanPopup popup = _serviceFactory.CreateSpendingPlanPopup();
            popup.Config = config;
            popup.ID = categoryId;
            popup.CreateCategories();
            popup.SaveConfig += SaveConfig;
            popup.UpdateBalanceBanner += UpdateBalanceBanner;
            container.Children.Add(popup);
        }

        //Removes the selected category from the config and the screen
        private void DeleteCategory(object sender, EventArgs e)
        {
            SpendingPlanCategory category = sender as SpendingPlanCategory;
            config.Categories.RemoveAt(category.Category.Id);

            //change the IDs of categories to match their new indices
            for (int i = category.Category.Id; i < config.Categories.Count(); i++)
            {
                config.Categories[i].Id--;
            }

            SaveConfig(null, null);
        }

        //Saves the changed config to the repository
        private void SaveConfig(object sender, EventArgs e)
        {
            categories.Children.Clear();
            CreateCategories();
            UpdateAmountsRemaining();
            _configService.SpendingPlanConfig = config;
        }

        //Converts parts of the config that couldn't be fully represented in json to the objects that will be used
        //Necessary for enumerations and DateTime objects
        private SpendingPlanConfig SetUpConfig(SpendingPlanConfig _config)
        {
            foreach(Category category in _config.Categories)
            {
                foreach(Subcategory subcategory in category.Subcategories)
                {
                    if (Enum.TryParse(subcategory.JsonAmount.Type, out AmountType amountType))
                    {
                        subcategory.Amount = new Amount(subcategory.JsonAmount.Number, amountType);
                    }
                    foreach (Transaction transaction in subcategory.Transactions)
                    {
                        if (DateTime.TryParse(transaction.DateString, out DateTime dateObject))
                        {
                            transaction.Date = dateObject;
                        }
                        if (Enum.TryParse(transaction.TypeString, out TransactionType typeObject))
                        {
                            transaction.Type = typeObject;
                        }
                    }
                }
            }
            return _config;
        }
    }
}