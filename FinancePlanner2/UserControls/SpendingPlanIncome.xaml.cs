﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancePlanner2.Model;
using FinancePlanner2.Services.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SpendingPlanIncome : ContentView
    {
        public event EventHandler SaveConfig;

        //IServiceFactory _serviceFactory;
        public Income Income { get; set; }
        public SpendingPlanConfig Config;

        public SpendingPlanIncome()
        {
            InitializeComponent();
        }

        //close window
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            ClosePopup();
        }

        //populate user input fields with current info
        public void PopulateFields()
        {
            amountEntry.Text = Income.Amount.ToString("0.00");
            timeFrameEntry.Text = Income.TimeFrame;
        }

        //make a new Income object from user input, save, and close window
        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            string amountString = amountEntry.Text;
            string timeFrame = timeFrameEntry.Text;

            decimal amount;
            try
            {
                amount = Convert.ToDecimal(amountString);
                amount = Math.Floor(amount * 100) / 100;
            }
            catch (FormatException) //if user enters something funky, revert to what it was before
            {
                amount = Income.Amount;
            }

            Income newIncome = new Income();
            newIncome.Amount = amount;
            newIncome.TimeFrame = timeFrame;

            //save
            Config.Income = newIncome;
            SaveConfig?.Invoke(this, null);

            //close window
            ClosePopup();
        }

        //close window
        private void ClosePopup()
        {
            popupLoadingView.IsVisible = false;
            popupLoadingView.InputTransparent = true;
            content.Children.Clear();
        }
    }
}