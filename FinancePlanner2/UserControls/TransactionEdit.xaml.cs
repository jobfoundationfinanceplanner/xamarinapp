﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancePlanner2.Model;
using FinancePlanner2.Services.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TransactionEdit : ContentView
    {
        public event EventHandler SaveConfig;

        //IServiceFactory _serviceFactory;
        public Transaction Transaction { get; set; }
        public int CategoryID { get; set; }
        public int SubcategoryID { get; set; }
        public SpendingPlanConfig Config;

        public TransactionEdit()
        {
            InitializeComponent();
        }

        //populates the user input fields with current info
        public void PopulateFields()
        {
            if(Transaction.Description != "New transaction") //user shouldn't have to manually delete the default value before entering a new one
            {
                descEntry.Text = Transaction.Description;
            }
            if(Transaction.Amount != 0)
            {
                amountEntry.Text = Transaction.Amount.ToString("0.00");
            }
            datePicker.Date = Transaction.Date;
            typePicker.ItemsSource = new List<string> {"Deposit","Withdrawal"};
            typePicker.SelectedIndex = (Transaction.Type == TransactionType.deposit) ? 0 : 1;
            if(Transaction.Medium != "")
            {
                mediumEntry.Text = Transaction.Medium;
            }
            recSwitch.IsToggled = Transaction.Reconciled;
            nfiSwitch.IsToggled = Transaction.NotFromIncome;

            typePicker_Changed(null, null);
        }

        //closes window
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            ClosePopup();
        }

        //creates a new Transaction object from user input, saves it, and closes window
        private void SaveButton_Clicked(object sender, EventArgs e)
        {
            string desc = descEntry.Text;
            string amountString = amountEntry.Text;
            DateTime date = datePicker.Date;
            int typeIndex = typePicker.SelectedIndex;
            string medium = mediumEntry.Text;
            bool reconciled = recSwitch.IsToggled;
            bool notFromIncome = nfiSwitch.IsToggled;

            if (desc == "" || desc == null) //if description is empty, reverts to whatever it was before
            {
                desc = Transaction.Description;
            }

            if (medium == "" || medium == null)
            {
                medium = Transaction.Medium;
            }

            decimal amountNum;
            try
            {
                amountNum = Convert.ToDecimal(amountString);
                amountNum = Math.Floor(amountNum * 100) / 100;
            }
            catch (FormatException) //if user enters something funky, revert to what it was before
            {
                amountNum = Transaction.Amount;
            }

            TransactionType type;
            if (typeIndex == 0)
            {
                type = TransactionType.deposit;
            }
            else
            {
                type = TransactionType.withdrawal;
            }

            Transaction newTr = new Transaction();
            newTr.Id = Transaction.Id;
            newTr.Description = desc;
            newTr.Amount = amountNum;
            newTr.Date = date;
            newTr.DateString = date.ToShortDateString();
            newTr.NotFromIncome = notFromIncome;
            newTr.Medium = medium;
            newTr.Reconciled = reconciled;
            newTr.Type = type;
            newTr.TypeString = type.ToString();

            //puts the new Transaction object into the config
            if (newTr.Id < Config.Categories[CategoryID].Subcategories[SubcategoryID].Transactions.Count() && newTr.Id >= 0)
            {
                Config.Categories[CategoryID].Subcategories[SubcategoryID].Transactions[Transaction.Id] = newTr;
            }
            else
            {
                Config.Categories[CategoryID].Subcategories[SubcategoryID].Transactions.Add(newTr);
            }

            //save
            SaveConfig?.Invoke(this, null);

            //close window
            ClosePopup();

        }

        //close window
        private void ClosePopup()
        {
            popupLoadingView.IsVisible = false;
            popupLoadingView.InputTransparent = true;
            content.Children.Clear();
        }

        //disables the "not for income" field if the selected type is deposit, and enables it otherwise
        void typePicker_Changed(object sender, EventArgs e)
        {
            if(typePicker.SelectedIndex == 0) //deposit
            {
                nfiLabel.IsVisible = false;
                nfiSwitch.IsVisible = false;
                nfiSwitch.InputTransparent = true;
            }
            else //withdrawal
            {
                nfiLabel.IsVisible = true;
                nfiSwitch.IsVisible = true;
                nfiSwitch.InputTransparent = false;
            }
        }
    }
}