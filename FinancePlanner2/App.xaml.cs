﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinancePlanner2
{
    public partial class App : Application
    {
        public static IServiceFactory ServiceFactory { get; private set; }
        public App()
        {
            ServiceFactory = new ServiceFactory();

            InitializeComponent();

            var page = ServiceFactory.HomePage;
            MainPage = new NavigationPage(page);
            NavigationPage.SetHasNavigationBar(page, false);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
