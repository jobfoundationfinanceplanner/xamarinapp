﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancePlanner2.Repository.Interfaces;
using PCLExt.FileStorage;
using PCLExt.FileStorage.Extensions;
using PCLExt.FileStorage.Folders;
using System.IO;
using System.Net;
using System.Reflection;

namespace FinancePlanner2.Repository
{
    public class ConfigRepository : IConfigRepository
    {
        //get the config from the appropriate location
        public string GetConfig(ConfigName configName)
        {
            string configString = configName.ToString() + ".json";
            var fileConfig = GetConfigFile(configString);
            if (fileConfig != null)
            {
                return fileConfig;
            }

            return GetConfigResource(configString);
        }

        //get data from file system
        public string GetConfigFile(string name)
        {
            var baseFolder = new LocalRootFolder();
            if (baseFolder.CheckExists(name) == ExistenceCheckResult.FileExists)
            {
                var configFile = baseFolder.GetFile(name);
                using (Stream stream = configFile.Open(PCLExt.FileStorage.FileAccess.Read))
                {
                    TextReader tr = new StreamReader(stream);
                    return tr.ReadToEnd();
                }
            }

            return null;
        }

        //get data from embedded resource
        public string GetConfigResource(string name)
        {
            using (Stream stream = Assembly.GetAssembly(this.GetType()).GetManifestResourceStream("FinancePlanner2.Resources." + name))
            {
                TextReader tr = new StreamReader(stream);
                return tr.ReadToEnd();
            }
        }

        //Update file system with user preference
        public void UpdateConfigFile(ConfigName name, string json)
        {
            var baseFolder = new LocalRootFolder();
            var configFile = baseFolder.CreateFile(name.ToString() + ".json", CreationCollisionOption.OpenIfExists);
            configFile.WriteAllText(json);
        }
    }
}
