﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancePlanner2.Repository.Interfaces
{
    public enum ConfigName { SpendingPlan };
    public interface IConfigRepository
    {
        //Retrieve the stored config
        string GetConfig(ConfigName configName);

        //Replace the stored config
        void UpdateConfigFile(ConfigName name, string data);
    }
}
